with
cust_table as (
select * from `biz_inno_sg_paotangpay.cust_register_profile`
where register_date <= '2023-01-31'
)
,
txn_table as (
select * from `inf-datalake.biz_inno_sg_paotangpay.vw_ptp_txn`
where (txn_dtm >='2023-01-01 00:00:00') and (txn_dtm <= '2023-01-31 23:59:59')
)
,
merge_table as (
select a.*,b.* from cust_table a
left join txn_table b
on a.paotang_pay_wallet_id = b.wallet_id
)
,
xx as (SELECT
paotang_pay_wallet_id,
sum(total_amount) as sum_amt
,RANK() OVER (ORDER BY sum(total_amount) desc) as ranking
FROM merge_table
group by 1
order by 2 )
,
yy as (select
paotang_pay_wallet_id,sum_amt,
case when ranking <= (10/100* (select max(ranking) from xx where sum_amt is not null )) THEN 'super_heavy'
when ranking <= (30/100* (select max(ranking) from xx where sum_amt is not null )) then 'heavy'
when ranking <= (60/100* (select max(ranking) from xx where sum_amt is not null )) then 'normal'
when sum_amt is not null then 'light'
else 'zero_txn'
end as rank_tier,
from xx )
,
zz as (
select cust_table.*,yy.rank_tier from cust_table
left join yy on cust_table.paotang_pay_wallet_id = yy.paotang_pay_wallet_id)
,
zzz as (select _citizen_id,paotang_pay_wallet_id,segment_age,segment_gender,segment_location, segment_occupation
,segment_customer_type,rank_tier from zz)

select 
'segment_age' as segment,segment_age,rank_tier
,count(distinct paotang_pay_wallet_id) as cnt_people
from zz
group by 2,3
union all 
select 
'segment_gender' as segment,segment_gender,rank_tier
,count(distinct paotang_pay_wallet_id) as cnt_people
from zz
group by 2,3







